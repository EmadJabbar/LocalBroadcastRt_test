package com.example.emadjabbar.localbroadcasttest.intent;

import android.content.Intent;

/**
 * Created by emadjabbar on 8/2/18.
 */

public class IntentRT extends Intent {

    private int priority;

    public IntentRT() {
        super();
    }

    public IntentRT(int priority) {
        super();
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}



