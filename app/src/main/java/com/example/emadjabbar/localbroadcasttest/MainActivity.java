package com.example.emadjabbar.localbroadcasttest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emadjabbar.localbroadcasttest.intent.IntentRT;
import com.example.emadjabbar.localbroadcasttest.localRt.LocalBroadcastManagerRT;

public class MainActivity extends AppCompatActivity {


    TextView resultTV;
    EditText sleepET;
    EditText numIPTET;
    EditText numThreadsET;
    EditText delayOnRecET;
    Button normBTN;
    Button RTBTN;

    private LocalBroadcastManager localBroadcastManager;
    private LocalBroadcastManagerRT localBroadcastManagerRT;
    private static int counter;
    private static int tCounter;
    private static int delay;
    private static long total;
    private static int NUM_OF_NORMAL_THREADS;
    private static long SLEEP_TIME;
    private static int NUM_OF_INTENT_PER_THREADS;
    int[] intentCounters;

    private static String INTENT_IDENTIFIER = "INTENT_";
    private static String ACTION = "TIMING_CUSTOM_ACTION";


    private BroadcastReceiver listener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            long t1 = System.nanoTime();
            Log.d("check1", ""+(System.nanoTime()-t1));
            tCounter++;
            Log.d("INTENT_ID", "ID--"+tCounter+"---:  "+ intent.getStringExtra("ID"));
            if (intent.getStringExtra("ID").equals("INTENT_"+(NUM_OF_NORMAL_THREADS-3))) {
                counter++;
                long diff = (System.nanoTime() - intent.getLongExtra("TIME", 0));
                total += diff;
//                Log.d("Timer>>>", counter + ":   " + diff);
                Log.d("Timer>>>", ":   " + diff);

                if (counter == NUM_OF_INTENT_PER_THREADS){
                    resultTV.setText("time in nS:  "+total);
                    Log.d("Timer>>>",  "final:   " + (total) + " ns");
                }
            }
            if (tCounter == NUM_OF_INTENT_PER_THREADS*NUM_OF_NORMAL_THREADS){
                Log.d("Timer>>>",  "final---------------------------");
                localBroadcastManager.unregisterReceiver(listener);
                localBroadcastManagerRT.unregisterReceiver(listener);
            }
            try {
                Thread.sleep(0,delay);
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.d("check2", ""+(System.nanoTime()-t1));

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sleepET = findViewById(R.id.sleepTime);
        numIPTET = findViewById(R.id.numOfIntentPerThread);
        numThreadsET = findViewById(R.id.numOfThreads);
        delayOnRecET = findViewById(R.id.onRecDelayTime);

        resultTV = findViewById(R.id.finalValue);

        normBTN = findViewById(R.id.doneInNormal);
        RTBTN = findViewById(R.id.doneInRT);

        localBroadcastManager= LocalBroadcastManager.getInstance(this);
        localBroadcastManagerRT = LocalBroadcastManagerRT.getInstance(this);




        normBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resultTV.setText("in progress...");

                counter = 0;
                tCounter = 0;
                total = 0;
                NUM_OF_NORMAL_THREADS= Integer.valueOf(numThreadsET.getText().toString());
                SLEEP_TIME = Long.valueOf(sleepET.getText().toString());
                NUM_OF_INTENT_PER_THREADS = Integer.valueOf(numIPTET.getText().toString());
                delay= Integer.valueOf(delayOnRecET.getText().toString());


                intentCounters = new int[NUM_OF_NORMAL_THREADS];
                for (int c=0; c<NUM_OF_NORMAL_THREADS;++c)
                    intentCounters[c] = 0;

                localBroadcastManager.registerReceiver(listener, new IntentFilter(ACTION));

                for (int i = 0; i < NUM_OF_NORMAL_THREADS; i++) {
                    final String id = INTENT_IDENTIFIER + i;
                    final Handler handler = new Handler();
                    final int finalI = i;
                    final Thread thread = new Thread() {
                        @Override
                        public void run() {
                            while (intentCounters[finalI] < NUM_OF_INTENT_PER_THREADS) {

                                    intentCounters[finalI]++;

                                    Intent intent = new Intent();
                                    intent.setAction(ACTION);
                                    intent.putExtra("ID", id);
                                    intent.putExtra("TIME", System.nanoTime());
                                    localBroadcastManager.sendBroadcast(intent);

                                try {
                                    Thread.sleep(SLEEP_TIME);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                handler.post(this);

                            }
                        }
                    };
                    if (i==NUM_OF_NORMAL_THREADS-3)
                        thread.setPriority(Thread.MAX_PRIORITY);
                    thread.start();
                }
            }
        });

        RTBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resultTV.setText("in progress...");

                counter = 0;
                tCounter=0;
                total = 0;
                NUM_OF_NORMAL_THREADS= Integer.valueOf(numThreadsET.getText().toString());
                SLEEP_TIME = Long.valueOf(sleepET.getText().toString());
                NUM_OF_INTENT_PER_THREADS = Integer.valueOf(numIPTET.getText().toString());
                delay= Integer.valueOf(delayOnRecET.getText().toString());



                intentCounters = new int[NUM_OF_NORMAL_THREADS];
                for (int c=0; c<NUM_OF_NORMAL_THREADS;++c)
                    intentCounters[c] = 0;

                localBroadcastManagerRT.registerReceiver(listener, new IntentFilter(ACTION));

                for (int i = 0; i < NUM_OF_NORMAL_THREADS; i++) {
                    final String id = INTENT_IDENTIFIER + i;
                    final Handler handler = new Handler();
                    final int finalI = i;
                    final Thread thread = new Thread() {
                        @Override
                        public void run() {
                            while (intentCounters[finalI] < NUM_OF_INTENT_PER_THREADS) {

                                    intentCounters[finalI]++;

                                    IntentRT intent = new IntentRT();
                                    if (finalI == NUM_OF_NORMAL_THREADS-3) {
                                        intent.setPriority(2);
                                    } else {
                                        intent.setPriority(5);
                                    }
                                    intent.setAction(ACTION);
                                    intent.putExtra("ID", id);
                                    intent.putExtra("TIME", System.nanoTime());
                                    localBroadcastManagerRT.sendBroadcast(intent);

                                try {
                                    Thread.sleep(SLEEP_TIME);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                handler.post(this);
                            }
                        }
                    };
                    if (i==NUM_OF_NORMAL_THREADS-3)
                        thread.setPriority(Thread.MAX_PRIORITY);
                    thread.start();
                }
            }
        });
    }

}
